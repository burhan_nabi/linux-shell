#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <signal.h>
#include <fcntl.h>

int main(int argc, char const *argv[])
{
	char *args[] = {"gcc", "-o", "shell", "2014031_A1_1.c", NULL};
	int cid = fork();
	if(cid == 0)
		execvp(args[0],args);
	int wc = wait(NULL);
	char *window[] = {"gnome-terminal", "-t", "MyShell", "-e", "./shell", NULL};
	execvp(window[0], window);
	return 0;
}