#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <signal.h>
#include <fcntl.h>


// Colours
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"
#define BOLDBLACK   "\033[1m\033[30m"    
#define BOLDRED     "\033[1m\033[31m"  
#define BOLDGREEN   "\033[1m\033[32m"    
#define BOLDYELLOW  "\033[1m\033[33m"     
#define BOLDBLUE    "\033[1m\033[34m"   
#define BOLDMAGENTA "\033[1m\033[35m"      
#define BOLDCYAN    "\033[1m\033[36m"   
#define BOLDWHITE   "\033[1m\033[37m"    

// Functions

void signalHandler(int sig_num);

/*
	Handles signals like Ctrl+C and Ctrl+V
*/
void signalHandler(int sig_num) {

	// Debugging info
	// printf("SIGNUM: %d\n",sig_num );


	if(sig_num == SIGINT) {
		// printf("Not Happening Bro\n");
		// printf("You can't (Ctrl +) C me\n");
		// printf("Press \"ENTER\" to continue \n");
		return;
	}

	if(sig_num == SIGTSTP ) {
	// For I/O asthetics
		printf("\n");
		int t = 2;
		fflush(stdout);
		while(t) {
			if(t!=1){
				printf(BOLDBLUE "Ending in  %d seconds\n",t);
				sleep(1);
			}
			else {
				printf(BOLDRED "DEAD\n");				
				sleep(1);
				exit(0);
			}
			t--;
		}
		exit(0);
	}
}


char *supported[] = { "cd", "history", "ps", "top", "ls", "wc", "pwd", "grep", "awk", "sed"
	, "gcc", "whoami", "help", "kill", "exit", "clear", "which", "echo", NULL};


int checkCommand(char *com) {
	int i = 0;

	while( supported[i] != NULL ) { 
		if(strcmp(supported[i], com)==0) {
			return 1;
		}
		i++;
	}
	return 0;
}	

#define KBLU  "\x1B[34m"


// Appends each new command typed by the user to a text file
int updateHistory(char *command) {

	FILE *file = fopen("history.txt","a+");
	if( file == NULL)
		printf("ERROR NO SUCH FILE\n");

	int returnValue = fseek(file, 0, SEEK_END);
	if( returnValue == 0) {
		fprintf( file, "%s\n", command);
	}
}

// Prints out the file which has the history
int displayHistory() {
	
	FILE *file = fopen("history.txt","a+");
	char buff[255];

	while( !feof(file) ) { 
		fgets(buff, 255, file);
		printf(KBLU "%s", buff);
	}
}

/*
	Returns a pointer to the array of token obtained on splitting a command
*/
char** splitIntoArgs(char *command) {
    int i ;

    // Memory for different tokens of the command
    // Assuming max 20
    char **save = malloc(sizeof(char *) * 20), *token;

    // For debugging
    // printf("Line:%s Delim:\"%s\" \n",line, " " );

    token = strtok(command, " ");

    for ( i = 0;  i>=0 ; i++) {

    	// For debugging
        // printf("%s\n", token);

        save[i] = strdup(token);
        token = strtok(NULL, " ");
        if(token == NULL)
        break;
    }
    save[++i] = NULL;
    return save;
}

int isASpecialCommand(char **args) {

    // If directory change command
    if(strcmp(args[0],"cd") == 0) {
        int returnValue = chdir( args[1]);
        if(returnValue == 0)
            return 1;
    }

    // History command
    if(strcmp(args[0],"history") == 0) {
        displayHistory();
        return 1;
    }
}


int hasPipes(char **args) {
	int i = 0;
	while( args[i++] != NULL ) { 
		if( args[i-1][0] == '|' )
			return 1;
	}
	return 0;
}


/**
 * Direction 0->read 1->write
 */
int redirectOutput(char **args) {

	int out = open("file.txt", O_RDWR|O_CREAT, 0600);
    if (-1 == out) { perror("opening cout.log"); return 255; }

	// int in = open("cout.log", O_RDWR|O_CREAT|O_APPEND, 0600);
 	// if (-1 == in) { perror("opening cin.log"); return 255; }

    int err = open("cerr.log", O_RDWR|O_CREAT|O_APPEND, 0600);
    if (-1 == err) { perror("opening cerr.log"); return 255; }

    int save_out = dup(fileno(stdout));
    int save_err = dup(fileno(stderr));
    // int save_in = dup(fileno(stdin));

    if (-1 == dup2(out, fileno(stdout))) { perror("cannot redirect stdout"); return 255; }
    if (-1 == dup2(err, fileno(stderr))) { perror("cannot redirect stderr"); return 255; }
	// if (-1 == dup2(in, fileno(stdin))) { perror("cannot redirect stdin"); return 255; }

    // puts("doing an ls or something now");
	int cid = fork();
	if (cid == 0) {
		execvp(args[0], args);
	}
	int wc =  wait(NULL);
    fflush(stdout); close(out);
    fflush(stderr); close(err);
	// fflush(stdin); close(in);

    dup2(save_out, fileno(stdout));
    dup2(save_err, fileno(stderr));
    // dup2(save_in, fileno(stdin));

    close(save_out);
    close(save_err);
    // close(save_in);
    // puts("back to normal output");

    return 0;
}

int redirectInput(char **args) {
	// int out = open("cout.log", O_RDWR|O_CREAT|O_APPEND, 0600);
 	// if (-1 == out) { perror("opening cout.log"); return 255; }

	// int in = open("file.txt", O_RDWR|O_CREAT|O_APPEND, 0600);
	int in = open("file.txt", O_RDWR|O_CREAT, 0600);
    if (-1 == in) { perror("opening cin.log"); return 255; }

    int err = open("cerr.log", O_RDWR|O_CREAT|O_APPEND, 0600);
    if (-1 == err) { perror("opening cerr.log"); return 255; }

    // int save_out = dup(fileno(stdout));
    int save_err = dup(fileno(stderr));
    int save_in = dup(fileno(stdin));

    // if (-1 == dup2(out, fileno(stdout))) { perror("cannot redirect stdout"); return 255; }
    if (-1 == dup2(err, fileno(stderr))) { perror("cannot redirect stderr"); return 255; }
	if (-1 == dup2(in, fileno(stdin))) { perror("cannot redirect stdin"); return 255; }

    // puts("doing an ls or something now");

	int cid = fork();
	if (cid == 0) {
		execvp(args[0], args);
	}
	int wc =  wait(NULL);
    // fflush(stdout); close(out);
    fflush(stderr); close(err);
	fflush(stdin); close(in);

    // dup2(save_out, fileno(stdout));
    dup2(save_err, fileno(stderr));
    dup2(save_in, fileno(stdin));

    // close(save_out);
    close(save_err);
    close(save_in);

   // puts("back to normal output");
    return 0;	
}

int handlePipes(char **args) {

	// printf("In here\n");
	char *arg =  malloc( sizeof(char *));
	int secIndex = 0, j=0, flag = 0, i, k=0;
	char **secondArgs = malloc( sizeof(char *) * 10);
	char **firstArgs = malloc( sizeof(char *) * 10);
	i = 0;

	while( arg != NULL ) {
		arg = args[i++]; 
		if(arg == NULL) break;
		if( strcmp(arg,"|") == 0 ) {
			secIndex = i;
			flag = 1;
			continue;
		}
		if (flag) {
			secondArgs[j++] = strdup(arg);
			// printf("%s ",secondArgs[j -1] );
		}else {
			firstArgs[k++] = strdup(arg);
			// printf("%s ",firstArgs[k -1] );
		}
	}
	secondArgs[j++] = NULL;
	firstArgs[k++] = NULL;

	char file[] = "temp";

	execPipe(firstArgs, &file, NULL, 1);
	execPipe(secondArgs, NULL, &file, 0);

}

// Choice : 0->InputRedirect 1->OutputRedirect 2->Both
int execPipe(char **args , char *OFile, char *IFile, int choice ) {

	srand(time(NULL));
	if( choice == 0) {
		// printf("Choice:%d\n", choice);
		redirectInput(args);
	}
	else if (choice == 1) {
		// printf("Choice:%d\n", choice);
		redirectOutput(args);
	}
	else if(choice == 2) {
		// printf("Choice:%d\n", choice);

	}else {
		// printf("Choice:%d\n", choice);

	}
}

int hasRedirects(char **args) {
	int i = 0;
	int out=0, in = 0;

	while( args[i++] != NULL ) { 
		if( args[i-1][0] == '>' )
			out = 1;
		if(args[i-1][0] == '<')
			in = 1;
	}
	
	if( in ) {
		execRedirect(args, -1);
		return 1;
	}
	if(out) {
		execRedirect(args, 1);
		return 1;
	}
	return 0;
}

// Dir : 1-> out -1-> in
int execRedirect(char **args, int dir) {
	char *arg =  malloc( sizeof(char *));
	char *file =  malloc( sizeof(char *));

	int i=0 , j=0;

	char **firstArgs = malloc( sizeof(char *) * 10);

	while( arg != NULL ) {
		arg = args[i++];
		if(NULL == arg) break;

		if( strcmp(arg, ">")==0 || strcmp(arg, "<")==0 ) {
			// printf("REDIRECTION\n");
			printf("%s\n",args[i]);
			file = (args[i++]);
			break;
		}
		else {
			firstArgs[j++] = strdup(arg);
		}
	}

	// printf("FILE: %s \n",file );

	if(dir == -1) {
		redInput(firstArgs,file);
	}else if(dir == 1) {
		redOutput(firstArgs,file);
	}
}

/**
 * Direction 0->read 1->write
 */
int redOutput(char **args, char *file) {

	// printf("%s\n", file);
	int out = open(file, O_RDWR|O_CREAT|O_APPEND, 0600);
    if (-1 == out) { perror(file); return 255; }

	// int in = open("cout.log", O_RDWR|O_CREAT|O_APPEND, 0600);
 	// if (-1 == in) { perror("opening cin.log"); return 255; }

    int err = open(file, O_RDWR|O_CREAT|O_APPEND, 0600);
    if (-1 == err) { perror(file); return 255; }

    int save_out = dup(fileno(stdout));
    int save_err = dup(fileno(stderr));
    // int save_in = dup(fileno(stdin));

    if (-1 == dup2(out, fileno(stdout))) { perror("cannot redirect stdout"); return 255; }
    if (-1 == dup2(err, fileno(stderr))) { perror("cannot redirect stderr"); return 255; }
	// if (-1 == dup2(in, fileno(stdin))) { perror("cannot redirect stdin"); return 255; }

    // puts("doing an ls or something now");
	int cid = fork();
	if (cid == 0) {
		execvp(args[0], args);
	}
	int wc =  wait(NULL);
    fflush(stdout); close(out);
    fflush(stderr); close(err);
	// fflush(stdin); close(in);

    dup2(save_out, fileno(stdout));
    dup2(save_err, fileno(stderr));
    // dup2(save_in, fileno(stdin));

    close(save_out);
    close(save_err);
    // close(save_in);
    // puts("back to normal output");

    return 0;
}

int redInput(char **args, char *file) {
	// int out = open("cout.log", O_RDWR|O_CREAT|O_APPEND, 0600);
 	// if (-1 == out) { perror("opening cout.log"); return 255; }

	// int in = open("file.txt", O_RDWR|O_CREAT|O_APPEND, 0600);
	int in = open(file, O_RDWR|O_CREAT|O_APPEND, 0600);
    if (-1 == in) { perror("opening cin.log"); return 255; }

    int err = open("cerr.log", O_RDWR|O_CREAT|O_APPEND, 0600);
    if (-1 == err) { perror("opening cerr.log"); return 255; }

    // int save_out = dup(fileno(stdout));
    int save_err = dup(fileno(stderr));
    int save_in = dup(fileno(stdin));

    // if (-1 == dup2(out, fileno(stdout))) { perror("cannot redirect stdout"); return 255; }
    if (-1 == dup2(err, fileno(stderr))) { perror("cannot redirect stderr"); return 255; }
	if (-1 == dup2(in, fileno(stdin))) { perror("cannot redirect stdin"); return 255; }

    // puts("doing an ls or something now");

	int cid = fork();
	if (cid == 0) {
		execvp(args[0], args);
	}
	int wc =  wait(NULL);
    // fflush(stdout); close(out);
    fflush(stderr); close(err);
	fflush(stdin); close(in);

    // dup2(save_out, fileno(stdout));
    dup2(save_err, fileno(stderr));
    dup2(save_in, fileno(stdin));

    // close(save_out);
    close(save_err);
    close(save_in);

   // puts("back to normal output");
    return 0;	
}


// For keeping track of the present working directory and User
char pwd[255];
char user[100];

int main(int argc, char const *argv[])
{
		
	printf(BOLDMAGENTA"Hello World!\n");
	printf( "Welcome To My Shell:\n");
	printf(KNRM "********************************************************************************\n");
	

	// Defining handlers for CTRL+C and CTRL+Z
	signal(SIGINT,signalHandler); // CTRL+C
	signal(SIGTSTP,signalHandler); // CTRL+Z

	char buffer[1024];
	char ch;
	int i;
	

	char **args;
	while( 1 ) { 

		getcwd(pwd, 254); 
		getlogin_r(user, 99);
		printf(BOLDYELLOW "[%s@seaShell %s] " KNRM, user, pwd);
		fgets(buffer, 1023, stdin);
		buffer[ strlen(buffer) -1 ] ='\0';

		if(strlen(buffer)==0)
			continue;

		// For debugging
		// printf("%s  Len: %d\n", buffer, (int) strlen(buffer) );
		int clear = 0;
		if( strlen(buffer)==1 && buffer[0]=='\f'){
			int cid = fork();
			if(cid == 0) {
				char *tempargs[] = {"clear", NULL}; 
				execvp(tempargs[0],tempargs );
			}
			int wc = wait(NULL);
			continue;
		}

		// Update history
		updateHistory(buffer);


		// Split into arguments
		args = splitIntoArgs(buffer);
		
		// int check = checkCommand(args[0]);
		// if(check)
		// 	printf("VALID COMMAND\n");
		// else{
		// 	printf(BOLDRED "INVALID COMMAND\nPlease Try Again\n" KNRM);
		// 	continue;
		// }
		if( strcmp(args[0], "help") == 0) {
			int cid = fork();
			if(cid == 0) {
				char *tempargs[] = {"cat","help.txt", NULL}; 
				execvp(tempargs[0],tempargs );
			}
			int wc = wait(NULL);
			continue;
		}
		// printf("HERE\n");

		int x = isASpecialCommand(args);
		if (x == 1)
			continue;

		int flag = 0;
		if( hasPipes(args)) {

			handlePipes(args);
			continue;
		}

		// For debugging
		// i = 0;
		// while( args[i++]!= NULL) {
		// 	printf("%s ",args[i-1] );
		// }
		// printf("\n");

		int rc = fork();
		// printf("PID: %d\n",rc );
		if(rc < 0 ) {
			fprintf(stderr, "Fork Failed\n" );
		}
		else if(rc==0) {
			// If child process launch a new process in its place

			// redirectOutput(args);	

			execvp(args[0], args);
		}else {
			// Parent process continues
			int wc = wait(NULL);
			continue;
		}

	}
	printf("This should never get printed!\n");	

	return 0;
}
