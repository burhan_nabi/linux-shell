#Shell Assignment

This is a linux shell i wrote in C as part of an OS course assignment.

Need to put a lot of work into it , to make it usable.

#Screen Shots:
![Alt text](/screenshots/1.png?raw=true "Startup Screen")
![Alt text](/screenshots/4.png?raw=true "Clear and History Commands")
![Alt text](/screenshots/5.png?raw=true "Top Command")
![Alt text](/screenshots/6.png?raw=true "Two variants of ls")
![Alt text](/screenshots/7.png?raw=true "Grep With Pipelinig")
