#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <signal.h>
#include <fcntl.h>
#include "signal.h"
#include "parse.h"
#include "history.h"

int hasPipes(char **args) {
	int i = 0;
	while( args[i++] != NULL ) { 
		if( args[i-1][0] == '|' )
			return 1;
	}
	return 0;
}


/**
 * Direction 0->read 1->write
 */
int redirectOutput(char **args) {

	int out = open("file.txt", O_RDWR|O_CREAT, 0600);
    if (-1 == out) { perror("opening cout.log"); return 255; }

	// int in = open("cout.log", O_RDWR|O_CREAT|O_APPEND, 0600);
 	// if (-1 == in) { perror("opening cin.log"); return 255; }

    int err = open("cerr.log", O_RDWR|O_CREAT|O_APPEND, 0600);
    if (-1 == err) { perror("opening cerr.log"); return 255; }

    int save_out = dup(fileno(stdout));
    int save_err = dup(fileno(stderr));
    // int save_in = dup(fileno(stdin));

    if (-1 == dup2(out, fileno(stdout))) { perror("cannot redirect stdout"); return 255; }
    if (-1 == dup2(err, fileno(stderr))) { perror("cannot redirect stderr"); return 255; }
	// if (-1 == dup2(in, fileno(stdin))) { perror("cannot redirect stdin"); return 255; }

    // puts("doing an ls or something now");
	int cid = fork();
	if (cid == 0) {
		execvp(args[0], args);
	}
	int wc =  wait(NULL);
    fflush(stdout); close(out);
    fflush(stderr); close(err);
	// fflush(stdin); close(in);

    dup2(save_out, fileno(stdout));
    dup2(save_err, fileno(stderr));
    // dup2(save_in, fileno(stdin));

    close(save_out);
    close(save_err);
    // close(save_in);
    // puts("back to normal output");

    return 0;
}

int redirectInput(char **args) {
	// int out = open("cout.log", O_RDWR|O_CREAT|O_APPEND, 0600);
 	// if (-1 == out) { perror("opening cout.log"); return 255; }

	// int in = open("file.txt", O_RDWR|O_CREAT|O_APPEND, 0600);
	int in = open("file.txt", O_RDWR|O_CREAT, 0600);
    if (-1 == in) { perror("opening cin.log"); return 255; }

    int err = open("cerr.log", O_RDWR|O_CREAT|O_APPEND, 0600);
    if (-1 == err) { perror("opening cerr.log"); return 255; }

    // int save_out = dup(fileno(stdout));
    int save_err = dup(fileno(stderr));
    int save_in = dup(fileno(stdin));

    // if (-1 == dup2(out, fileno(stdout))) { perror("cannot redirect stdout"); return 255; }
    if (-1 == dup2(err, fileno(stderr))) { perror("cannot redirect stderr"); return 255; }
	if (-1 == dup2(in, fileno(stdin))) { perror("cannot redirect stdin"); return 255; }

    // puts("doing an ls or something now");

	int cid = fork();
	if (cid == 0) {
		execvp(args[0], args);
	}
	int wc =  wait(NULL);
    // fflush(stdout); close(out);
    fflush(stderr); close(err);
	fflush(stdin); close(in);

    // dup2(save_out, fileno(stdout));
    dup2(save_err, fileno(stderr));
    dup2(save_in, fileno(stdin));

    // close(save_out);
    close(save_err);
    close(save_in);

   // puts("back to normal output");
    return 0;	
}

int handlePipes(char **args) {

	// printf("In here\n");
	char *arg =  malloc( sizeof(char *));
	int secIndex = 0, j=0, flag = 0, i, k=0;
	char **secondArgs = malloc( sizeof(char *) * 10);
	char **firstArgs = malloc( sizeof(char *) * 10);
	i = 0;

	while( arg != NULL ) {
		arg = args[i++]; 
		if(arg == NULL) break;
		if( strcmp(arg,"|") == 0 ) {
			secIndex = i;
			flag = 1;
			continue;
		}
		if (flag) {
			secondArgs[j++] = strdup(arg);
			// printf("%s ",secondArgs[j -1] );
		}else {
			firstArgs[k++] = strdup(arg);
			// printf("%s ",firstArgs[k -1] );
		}
	}
	secondArgs[j++] = NULL;
	firstArgs[k++] = NULL;

	char file[] = "temp";

	execPipe(firstArgs, &file, NULL, 1);
	execPipe(secondArgs, NULL, &file, 0);

}

// Choice : 0->InputRedirect 1->OutputRedirect 2->Both
int execPipe(char **args , char *OFile, char *IFile, int choice ) {

	srand(time(NULL));
	if( choice == 0) {
		// printf("Choice:%d\n", choice);
		redirectInput(args);
	}
	else if (choice == 1) {
		// printf("Choice:%d\n", choice);
		redirectOutput(args);
	}
	else if(choice == 2) {
		// printf("Choice:%d\n", choice);

	}else {
		// printf("Choice:%d\n", choice);

	}
}

int hasRedirects(char **args) {
	int i = 0;
	int out=0, in = 0;

	while( args[i++] != NULL ) { 
		if( args[i-1][0] == '>' )
			out = 1;
		if(args[i-1][0] == '<')
			in = 1;
	}
	
	if( in ) {
		execRedirect(args, -1);
		return 1;
	}
	if(out) {
		execRedirect(args, 1);
		return 1;
	}
	return 0;
}

// Dir : 1-> out -1-> in
int execRedirect(char **args, int dir) {
	char *arg =  malloc( sizeof(char *));
	char *file =  malloc( sizeof(char *));

	int i=0 , j=0;

	char **firstArgs = malloc( sizeof(char *) * 10);

	while( arg != NULL ) {
		arg = args[i++];
		if(NULL == arg) break;

		if( strcmp(arg, ">")==0 || strcmp(arg, "<")==0 ) {
			// printf("REDIRECTION\n");
			printf("%s\n",args[i]);
			file = (args[i++]);
			break;
		}
		else {
			firstArgs[j++] = strdup(arg);
		}
	}

	// printf("FILE: %s \n",file );

	if(dir == -1) {
		redInput(firstArgs,file);
	}else if(dir == 1) {
		redOutput(firstArgs,file);
	}
}

/**
 * Direction 0->read 1->write
 */
int redOutput(char **args, char *file) {

	// printf("%s\n", file);
	int out = open(file, O_RDWR|O_CREAT|O_APPEND, 0600);
    if (-1 == out) { perror(file); return 255; }

	// int in = open("cout.log", O_RDWR|O_CREAT|O_APPEND, 0600);
 	// if (-1 == in) { perror("opening cin.log"); return 255; }

    int err = open(file, O_RDWR|O_CREAT|O_APPEND, 0600);
    if (-1 == err) { perror(file); return 255; }

    int save_out = dup(fileno(stdout));
    int save_err = dup(fileno(stderr));
    // int save_in = dup(fileno(stdin));

    if (-1 == dup2(out, fileno(stdout))) { perror("cannot redirect stdout"); return 255; }
    if (-1 == dup2(err, fileno(stderr))) { perror("cannot redirect stderr"); return 255; }
	// if (-1 == dup2(in, fileno(stdin))) { perror("cannot redirect stdin"); return 255; }

    // puts("doing an ls or something now");
	int cid = fork();
	if (cid == 0) {
		execvp(args[0], args);
	}
	int wc =  wait(NULL);
    fflush(stdout); close(out);
    fflush(stderr); close(err);
	// fflush(stdin); close(in);

    dup2(save_out, fileno(stdout));
    dup2(save_err, fileno(stderr));
    // dup2(save_in, fileno(stdin));

    close(save_out);
    close(save_err);
    // close(save_in);
    // puts("back to normal output");

    return 0;
}

int redInput(char **args, char *file) {
	// int out = open("cout.log", O_RDWR|O_CREAT|O_APPEND, 0600);
 	// if (-1 == out) { perror("opening cout.log"); return 255; }

	// int in = open("file.txt", O_RDWR|O_CREAT|O_APPEND, 0600);
	int in = open(file, O_RDWR|O_CREAT|O_APPEND, 0600);
    if (-1 == in) { perror("opening cin.log"); return 255; }

    int err = open("cerr.log", O_RDWR|O_CREAT|O_APPEND, 0600);
    if (-1 == err) { perror("opening cerr.log"); return 255; }

    // int save_out = dup(fileno(stdout));
    int save_err = dup(fileno(stderr));
    int save_in = dup(fileno(stdin));

    // if (-1 == dup2(out, fileno(stdout))) { perror("cannot redirect stdout"); return 255; }
    if (-1 == dup2(err, fileno(stderr))) { perror("cannot redirect stderr"); return 255; }
	if (-1 == dup2(in, fileno(stdin))) { perror("cannot redirect stdin"); return 255; }

    // puts("doing an ls or something now");

	int cid = fork();
	if (cid == 0) {
		execvp(args[0], args);
	}
	int wc =  wait(NULL);
    // fflush(stdout); close(out);
    fflush(stderr); close(err);
	fflush(stdin); close(in);

    // dup2(save_out, fileno(stdout));
    dup2(save_err, fileno(stderr));
    dup2(save_in, fileno(stdin));

    // close(save_out);
    close(save_err);
    close(save_in);

   // puts("back to normal output");
    return 0;	
}
