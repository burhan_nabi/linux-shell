#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <signal.h>
#include "signal.h"
#include "parse.h"
#include "history.h"


char *supported[] = { "cd", "cat", "history", "ps", "top", "ls", "wc", "pwd", "grep", "awk", "sed"
	, "gcc", "whoami", "help", "kill", "exit", "clear", "which", "echo", NULL};


int checkCommand(char *com) {
	int i = 0;

	while( supported[i] != NULL ) { 
		if(strcmp(supported[i], com)==0) {
			return 1;
		}
		i++;
	}
	return 0;
}	

