#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <signal.h>
#include "signal.h"
#include "parse.h"
#include "history.h"
#include "pipe.h"
#include "commands.h"


// Colours
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"
#define BOLDBLACK   "\033[1m\033[30m"    
#define BOLDRED     "\033[1m\033[31m"  
#define BOLDGREEN   "\033[1m\033[32m"    
#define BOLDYELLOW  "\033[1m\033[33m"     
#define BOLDBLUE    "\033[1m\033[34m"   
#define BOLDMAGENTA "\033[1m\033[35m"      
#define BOLDCYAN    "\033[1m\033[36m"   
#define BOLDWHITE   "\033[1m\033[37m"    


// For keeping track of the present working directory and User
char pwd[255];
char user[100];

int main(int argc, char const *argv[])
{
	label: 	
	printf(BOLDMAGENTA"Hello World!\n");
	printf( "Welcome To My Shell:\n");
	printf(KNRM "********************************************************************************\n");
	

	// Defining handlers for CTRL+C and CTRL+Z
	signal(SIGINT,signalHandler); // CTRL+C
	signal(SIGTSTP,signalHandler); // CTRL+Z

	char buffer[1024];
	char ch;
	int i = 0;
	

	char **args;
	while( 1 ) { 
		start:
		i = 0;
		getcwd(pwd, 254); 
		getlogin_r(user, 99);
		printf(BOLDYELLOW "[%s@seaShell %s] " KNRM, user, pwd);

		fgets(buffer, 1023, stdin);
		buffer[ strlen(buffer) -1 ] ='\0';

		// while( (ch=getchar()) && ch!='\n') {
		// 	buffer[i++] = ch;
		// }
		// buffer[i++]='\0';


		if(strlen(buffer)==0)
			continue;

		// For debugging
		// printf("%s  Len: %d\n", buffer, (int) strlen(buffer) );
		int clear = 0;
		if( strlen(buffer)==1 && buffer[0]=='\f'){
			int cid = fork();
			if(cid == 0) {
				char *tempargs[] = {"clear", NULL}; 
				execvp(tempargs[0],tempargs );
			}
			int wc = wait(NULL);
			continue;
		}

		// Update history
		updateHistory(buffer);


		// Split into arguments
		args = splitIntoArgs(buffer);
		
		int check = checkCommand(args[0]);
		if(check)
			// printf("VALID COMMAND\n");
			;
		else{
			printf(BOLDRED "INVALID COMMAND\nPlease Try Again\n" KNRM);
			continue;
		}
		if( strcmp(args[0], "help") == 0) {
			int cid = fork();
			if(cid == 0) {
				char *tempargs[] = {"cat","help.txt", NULL}; 
				execvp(tempargs[0],tempargs );
			}
			int wc = wait(NULL);
			continue;
		}
		// printf("HERE\n");

		int x = isASpecialCommand(args);
		if (x == 1)
			continue;

		int flag = 0;
		
		if(!hasRedirects(args))
			;
		else {
			continue;
		}

		if( hasPipes(args)) {

			handlePipes(args);
			continue;
		}

		// For debugging
		// i = 0;
		// while( args[i++]!= NULL) {
		// 	printf("%s ",args[i-1] );
		// }
		// printf("\n");

		int rc = fork();
		// printf("PID: %d\n",rc );
		if(rc < 0 ) {
			fprintf(stderr, "Fork Failed\n" );
		}
		else if(rc==0) {
			// If child process launch a new process in its place

			// redirectOutput(args);	

			execvp(args[0], args);
		}else {
			// Parent process continues
			int wc = wait(NULL);
			continue;
		}

	}
	printf("This should never get printed!\n");	

	return 0;
}
