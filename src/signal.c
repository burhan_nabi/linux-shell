#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <signal.h>

// Colours
#define BOLDRED     "\033[1m\033[31m"  
#define BOLDBLUE    "\033[1m\033[34m"   


void signalHandler(int sig_num);

/*
	Handles signals like Ctrl+C and Ctrl+V
*/
void signalHandler(int sig_num) {

	// Debugging info
	// printf("SIGNUM: %d\n",sig_num );


	if(sig_num == SIGINT) {
		// printf("Not Happening Bro\n");
		// printf("You can't (Ctrl +) C me\n");
		// printf("Press \"ENTER\" to continue \n");
		return;
	}

	if(sig_num == SIGTSTP ) {
	// For I/O asthetics
		printf("\n");
		int t = 2;
		fflush(stdout);
		while(t) {
			if(t!=1){
				printf(BOLDBLUE "Ending in  %d seconds\n",t);
				sleep(1);
			}
			else {
				printf(BOLDRED "DEAD\n");				
				sleep(1);
				exit(0);
			}
			t--;
		}
		exit(0);
	}
}
