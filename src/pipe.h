int hasPipes(char **args);
int handlePipes(char **args);
int redirectOutput(char **args);
int redirectInput(char **args);
int redInput(char **args, char *file);
int redOutput(char **args, char *file);
