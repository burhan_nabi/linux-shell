#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <signal.h>
#include "signal.h"
#include "parse.h"

#define KBLU  "\x1B[34m"


// Appends each new command typed by the user to a text file
int updateHistory(char *command) {

	FILE *file = fopen("history.txt","a+");
	if( file == NULL)
		printf("ERROR NO SUCH FILE\n");

	int returnValue = fseek(file, 0, SEEK_END);
	if( returnValue == 0) {
		fprintf( file, "%s\n", command);
	}
}

// Prints out the file which has the history
int displayHistory() {
	
	FILE *file = fopen("history.txt","a+");
	char buff[255];

	while( !feof(file) ) { 
		fgets(buff, 255, file);
		printf(KBLU "%s", buff);
	}
}