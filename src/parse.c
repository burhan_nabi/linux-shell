#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <signal.h>

/*
	Returns a pointer to the array of token obtained on splitting a command
*/
char** splitIntoArgs(char *command) {
    int i ;

    // Memory for different tokens of the command
    // Assuming max 20
    char **save = malloc(sizeof(char *) * 20), *token;

    // For debugging
    // printf("Line:%s Delim:\"%s\" \n",line, " " );

    token = strtok(command, " ");

    for ( i = 0;  i>=0 ; i++) {

    	// For debugging
        // printf("%s\n", token);

        save[i] = strdup(token);
        token = strtok(NULL, " ");
        if(token == NULL)
        break;
    }
    save[++i] = NULL;
    return save;
}

int isASpecialCommand(char **args) {

    // If directory change command
    if(strcmp(args[0],"cd") == 0) {
        int returnValue = chdir( args[1]);
        if(returnValue == 0)
            return 1;
    }

    // History command
    if(strcmp(args[0],"history") == 0) {
        displayHistory();
        return 1;
    }
}
